import { MERCADO_PUBLICO_API_BASE_URL, MERCADO_PUBLICO_API_TICKET } from '../config/environment';
import { getRequest } from '../modules/requests';
import { makeLog } from '../modules/utils';
import Purchase from '../models/purchase/purchase';
import { IPurchaseFields } from '../models/purchase/purchase.schemas';

type TGetPurchaseService = (purchaseID: string) => Promise<IPurchaseFields | null>;

/**
 * Service to get a purchase by the given identifier.
 * 
 * @typedef {TGetPurchaseService} 
 */
export const getPurchase: TGetPurchaseService = purchaseID => {
  const url = `${MERCADO_PUBLICO_API_BASE_URL}/ordenesdecompra.json?ticket=${MERCADO_PUBLICO_API_TICKET}&codigo=${purchaseID}`;

  return getRequest(url)
    .then(response => JSON.parse(response))
    .then(jsonResponse => new Purchase(jsonResponse).decorate())
    .then(purchase => {
      makeLog('TGetPurchaseService', { purchase, url });

      return purchase;
    });
};
