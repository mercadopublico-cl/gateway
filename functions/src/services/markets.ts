import { MERCADO_PUBLICO_PORTAL_BASE_URL } from '../config/environment';
import { getRequest, postRequest } from '../modules/requests';

type TGetMarketService = (marketID: string) => Promise<any>;

/**
 * Service to get a market by the given identifier.
 * 
 * @typedef {TGetMarketService} 
 */
export const getMarket: TGetMarketService = marketID =>
  postRequest(`${MERCADO_PUBLICO_PORTAL_BASE_URL}/BuscarRubroPorCodigo`, { idRubro: marketID });

type TGetMarketsService = (pattern: string) => Promise<any[]>;

/**
 * Service to get a list of markets by the given pattern.
 * 
 * @typedef {TGetMarketsService} 
 */
export const getMarkets: TGetMarketsService = pattern =>
  getRequest(`${MERCADO_PUBLICO_PORTAL_BASE_URL}/BuscarRubros?q=${pattern}`);
