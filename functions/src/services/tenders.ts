import {
  MERCADO_PUBLICO_API_BASE_URL,
  MERCADO_PUBLICO_API_TICKET,
  MERCADO_PUBLICO_PORTAL_BASE_URL,
  MERCADO_PUBLICO_RSS_BASE_URL,
} from '../config/environment';
import { HTML_TENDER_PATTERN, XML_TENDER_PATTERN } from '../config/regex';
import { MAX_LIST_RESULTS } from '../config/settings';
import { debounceRequest, errorRequest, getRequest, postRequest } from '../modules/requests';
import { convertStringToNumber, makeLog, parseDataFromText, sliceList } from '../modules/utils';
import Tender from '../models/tender/tender';
import { ITenderFields } from '../models/tender/tender.schemas';
import { ITenderFilters, ITenderBody, TenderAmount, TenderSort, TenderStatus, TenderType } from '../models/tender/tender.filters';

type TGetTenderService = (tenderID: string) => Promise<ITenderFields | null>;

/**
 * Service to get a tender by the given identifier.
 * 
 * @typedef {TGetTenderService}
 */
export const getTender: TGetTenderService = tenderID => {
  const url = `${MERCADO_PUBLICO_API_BASE_URL}/licitaciones.json?ticket=${MERCADO_PUBLICO_API_TICKET}&codigo=${tenderID}`;

  return getRequest(url)
    .then(response => JSON.parse(response))
    .then(jsonResponse => errorRequest(() => jsonResponse, jsonResponse.Codigo === 10500))
    .then(jsonResponse => new Tender(jsonResponse).decorate())
    .then(tender => {
      makeLog('TGetTenderService', { tender, url });

      return tender;
    })
    .catch(() => debounceRequest(() => getTender(tenderID)));
};

type TGetTendersFromFeedService = (
  orgID: string,
  maxResults?: string,
  startIndex?: string,
  endIndex?: string,
) => Promise<string[]>;

/**
 * Service to get a list of tenders from the official RSS Feed.
 * 
 * @typedef {TGetTendersFromFeedService}
 */
export const getTendersFromFeed: TGetTendersFromFeedService = (orgID, maxResults = '', startIndex = '', endIndex = '') => {
  const url = `${MERCADO_PUBLICO_RSS_BASE_URL}?OrgCode=${orgID}`;
  const resultsLimit = convertStringToNumber(maxResults, MAX_LIST_RESULTS);

  return getRequest(url)
    .then(response => parseDataFromText(response, XML_TENDER_PATTERN))
    .then(response => sliceList(response, 0, resultsLimit))
    .then(response => sliceList(response, startIndex, endIndex))
    .then(tendersIDs => {
      makeLog('TGetTendersFromFeedService', { orgID, tendersIDs, url });

      return tendersIDs;
    });
}

type TGetTendersFromPortalService = (
  filters: ITenderFilters,
  maxResults?: string,
  startIndex?: string,
  endIndex?: string,
  records?: string[],
  page?: number,
) => Promise<string[]>;

/**
 * Service to get a list of tenders from the official Portal.
 * 
 * @typedef {TGetTendersFromPortalService}
 */
export const getTendersFromPortal: TGetTendersFromPortalService = (
  filters,
  maxResults = '',
  startIndex = '',
  endIndex = '',
  records = [],
  page = 1,
) => {
  const url = `${MERCADO_PUBLICO_PORTAL_BASE_URL}/Buscar`;
  const body: ITenderBody = {
    codigoRegion: filters.region || '-1',
    compradores: [],
    esPublicoMontoEstimado: TenderAmount[filters.montoPublicado || ''] || null,
    fechaFin: filters.fechaFin || null,
    fechaInicio: filters.fechaInicio || null,
    garantias: null,
    idEstado: TenderStatus[(filters.estado || 'all') as keyof typeof TenderStatus],
    idOrden: TenderSort[filters.order || ''] || '5',
    idTipoFecha: filters.fecha ? filters.fecha.split(',') : [],
    idTipoLicitacion: TenderType[(filters.tipo || 'all') as keyof typeof TenderType],
    montoEstimadoTipo: [0],
    pagina: page || 1,
    proveedores: [],
    registrosPorPagina: 10,
    rubros: filters.rubros ? filters.rubros.split(',') : [],
    textoBusqueda: filters.texto || '',
  };
  const resultsLimit = convertStringToNumber(maxResults, MAX_LIST_RESULTS);

  return postRequest(url, body)
    .then(response => parseDataFromText(response, HTML_TENDER_PATTERN))
    .then(response => {
      const updatedRecords = records.concat(response);

      if (response.length === 0 || updatedRecords.length >= resultsLimit) {
        const tendersIDs = sliceList(updatedRecords, startIndex, endIndex);

        makeLog('TGetTendersFromPortalService: ', { body, tendersIDs, url });

        return tendersIDs;
      }

      return getTendersFromPortal(filters, maxResults, startIndex, endIndex, updatedRecords, page + 1);
    });
};
