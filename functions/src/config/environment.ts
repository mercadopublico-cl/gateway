export const MERCADO_PUBLICO_API_BASE_URL = 'http://api.mercadopublico.cl/servicios/v1/publico';
export const MERCADO_PUBLICO_API_TICKET = 'DE612BEC-8A92-4DBE-8DF7-71566B0A0548';
export const MERCADO_PUBLICO_PORTAL_BASE_URL = 'http://www.mercadopublico.cl/BuscarLicitacion/Home';
export const MERCADO_PUBLICO_RSS_BASE_URL = 'http://www.mercadopublico.cl/Portal/feed.aspx';
