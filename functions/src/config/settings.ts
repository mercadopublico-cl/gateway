export const MAX_LIST_RESULTS = 100;
export const REQUEST_DEBOUNCE_TIME = 2000;
export const SLICE_LIST_END_INDEX = 10;
export const SLICE_LIST_START_INDEX = 0;
