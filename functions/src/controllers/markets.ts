import { getMarket, getMarkets } from '../services/markets';

type TMarketGetByIDController = (marketID: string) => Promise<any>; 

/**
 * The controller to get a market by identifier.
 * 
 * Example: 72131700.
 * 
 * @typedef {TMarketGetByIDController}
 */
export const getByID: TMarketGetByIDController = (marketID = '') => getMarket(marketID);

type TMarketSearchController = () => Promise<any[]>; 

/**
 * The controller to search markets by text pattern.
 * 
 * Example: cons.
 * 
 * @typedef {TMarketSearchController}
 */
export const search: TMarketSearchController = (pattern = '') => getMarkets(pattern);
