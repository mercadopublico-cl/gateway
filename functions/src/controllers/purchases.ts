import { getPurchase } from '../services/purchases';

type TPurchaseGetByIDController = (purchaseID: string) => Promise<any>;

/**
 * The controller to get a purchase by identifier.
 * 
 * @typedef {TPurchaseGetByIDController}
 */
export const getByID: TPurchaseGetByIDController = purchaseID => getPurchase(purchaseID);
