import { recursiveChainFromList } from '../modules/utils';
import { getTender, getTendersFromFeed, getTendersFromPortal } from '../services/tenders';
import { ITenderFields } from '../models/tender/tender.schemas';
import { ITenderFilters } from '../models/tender/tender.filters';

interface TCommonFilters {
  indiceFin?: string;
  indiceInicio?: string;
  totalMuestra?: string;
}

type TTenderGetFromFeedController = (orgID: string, filters: TCommonFilters) => Promise<ITenderFields[]>;

/**
 * The controller to get markets from the RSS Feed.
 *
 * @typedef {TTenderGetFromFeedController}
 */
export const getFromFeed: TTenderGetFromFeedController = (orgID, filters) => {
  const { indiceFin = '', indiceInicio = '', totalMuestra = '' } = filters;

  return getTendersFromFeed(orgID, totalMuestra, indiceInicio, indiceFin)
    .then(tendersIDs => recursiveChainFromList(tenderID => getTender(tenderID), tendersIDs));
};

type TTenderGetFromPortalController = (filters: TCommonFilters & ITenderFilters) => Promise<ITenderFields[]>;

/**
 * The controller to get markets from the Portal.
 * 
 * @typedef {TTenderGetFromPortalController}
 */
export const getFromPortal: TTenderGetFromPortalController = filters => {
  const { indiceFin = '', indiceInicio = '', totalMuestra = '', ...restFilters } = filters;

  return getTendersFromPortal(restFilters, totalMuestra, indiceInicio, indiceFin)
    .then(tendersIDs => recursiveChainFromList(tenderID => getTender(tenderID), tendersIDs));
}
