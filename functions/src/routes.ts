import express from 'express';
import * as purchasesResources from './resources/purchases';
import * as tendersResources from './resources/tenders';

const router = express.Router();

router.get('/purchases/:purchaseID', purchasesResources.getByID);

router.get('/tenders/feed/:orgID', tendersResources.getFromFeed);

router.get('/tenders/portal', tendersResources.getFromPortal);

export default router;
