import { composeDecorators } from '../../modules/utils';
import {
  decorateBoolean,
  decorateDate,
  decorateDictionary,
  decorateEmpty,
  decoratePhoneNumber,
} from '../decorators';

const Awards = {
  1: 'Autorización',
  2: 'Resolución',
  3: 'Acuerdo',
  4: 'Decreto',
  5: 'Otros',
};
const Contracts = {
  '0': 'sin_documentacion',
  '1': 'Contrato Requiere subscripción',
  '2': 'Contrato Formaliza con OC',
};
const Estimations = {
  1: 'Presupuesto Disponible',
  2: 'Precio Referencial',
  3: 'Monto no es posible de estimar',
};
const Modalities = {
  0: 'sin_documentacion',
  1: 'Pago a 30 días',
  2: 'Pago a 30, 60 y 90 días',
  3: 'Pago al día',
  4: 'Pago Anual',
  5: 'Pago Bimensual',
  6: 'Pago Contra Entrega Conforme',
  7: 'Pagos Mensuales',
  8: 'Pago Por Estado de Avance',
  9: 'Pago Trimestral',
  10: 'Pago a 60 días',
};
const TimeUnits = {
  0: 'sin_documentacion',
  1: 'Horas',
  2: 'Días',
  3: 'Semanas',
  4: 'Meses',
  5: 'Años',
};

export interface ITenderFields {
  'CantidadReclamos': string;
  'Codigo': string;
  'CodigoOrganismoComprador': string;
  'ComunaUnidadComprador': string;
  'Contrato': string;
  'Descripcion': string;
  'DiasCierreLicitacion': string;
  'DireccionEntrega': string;
  'DireccionUnidadComprador': string;
  'DireccionVisita': string;
  'EmailResponsableContrato': string;
  'EmailResponsablePago': string;
  'Estado': string;
  'EstadoPublicidadOfertas': string;
  'Estimacion': string;
  'EsRenovable': string;
  'FechaAdjudicacion': string;
  'FechaCierre': string;
  'FechaCreacion': string;
  'FechaEstimadaAdjudicacion': string;
  'FechaEstimadaFirma': string;
  'FechaFinal': string;
  'FechaInicio': string;
  'FechaPublicacion': string;
  'FechaPubRespuestas': string;
  'FechaVisitaTerreno': string;
  'FonoResponsableContrato': string;
  'FuenteFinanciamiento': string;
  'Modalidad': string;
  'MontoEstimado': string;
  'Nombre': string;
  'NombreOrganismoComprador': string;
  'NombreResponsableContrato': string;
  'NombreResponsablePago': string;
  'NombreUsuarioComprador': string;
  'NumeroAdjudicacion': string;
  'NumeroOferentesAdjudicacion': string;
  'Obras': string;
  'PeriodoTiempoRenovacion': string;
  'ProhibicionContratacion': string;
  'RegionUnidadComprador': string;
  'RutUnidadComprador': string;
  'SubContratacion': string;
  'TiempoDuracionContrato': string;
  'Tipo': string;
  'TipoAdjudicacion': string;
  // 'TipoPago': string; // undocumented
  'TomaRazon': string;
  // 'UnidadTiempoContratoLicitacion': string; // undocumented
  'UnidadTiempoDuracionContrato': string;
  'UrlActaAdjudicacion': string;
  // 'ValorTiempoRenovacion': string; // undocumented
  // 'VisibilidadMonto': string; // undocumented
}

export interface ITenderFieldsDecorators {
  [key: string]: {
    decorate: any;
    path: string;
  };
}

export const TenderFieldsDecorators: ITenderFieldsDecorators = {
  'CantidadReclamos': {
    decorate: composeDecorators(decorateEmpty),
    path: 'CantidadReclamos',
  },
  'Codigo': {
    decorate: composeDecorators(decorateEmpty),
    path: 'CodigoExterno',
  },
  'CodigoOrganismoComprador': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Comprador.CodigoOrganismo',
  },
  'ComunaUnidadComprador': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Comprador.ComunaUnidad',
  },
  'Contrato': {
    decorate: composeDecorators(decorateEmpty, decorateDictionary(Contracts)),
    path: 'Contrato',
  },
  'Descripcion': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Descripcion',
  },
  'DiasCierreLicitacion': {
    decorate: composeDecorators(decorateEmpty),
    path: 'DiasCierreLicitacion',
  },
  'DireccionEntrega': {
    decorate: composeDecorators(decorateEmpty),
    path: 'DireccionEntrega',
  },
  'DireccionUnidadComprador': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Comprador.DireccionUnidad',
  },
  'DireccionVisita': {
    decorate: composeDecorators(decorateEmpty),
    path: 'DireccionVisita',
  },
  'EmailResponsableContrato': {
    decorate: composeDecorators(decorateEmpty),
    path: 'EmailResponsableContrato',
  },
  'EmailResponsablePago': {
    decorate: composeDecorators(decorateEmpty),
    path: 'EmailResponsablePago',
  },
  'Estado': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Estado',
  },
  'EstadoPublicidadOfertas': {
    decorate: composeDecorators(decorateEmpty, decorateBoolean),
    path: 'EstadoPublicidadOfertas',
  },
  'Estimacion': {
    decorate: composeDecorators(decorateEmpty, decorateDictionary(Estimations)),
    path: 'Estimacion',
  },
  'EsRenovable': {
    decorate: composeDecorators(decorateEmpty, decorateBoolean),
    path: 'EsRenovable',
  },
  'FechaAdjudicacion': {
    decorate: composeDecorators(decorateEmpty, decorateDate),
    path: 'Fechas.FechaAdjudicacion',
  },
  'FechaCierre': {
    decorate: composeDecorators(decorateEmpty, decorateDate),
    path: 'Fechas.FechaCierre',
  },
  'FechaCreacion': {
    decorate: composeDecorators(decorateEmpty, decorateDate),
    path: 'Fechas.FechaCreacion',
  },
  'FechaEstimadaAdjudicacion': {
    decorate: composeDecorators(decorateEmpty, decorateDate),
    path: 'Fechas.FechaEstimadaAdjudicacion',
  },
  'FechaEstimadaFirma': {
    decorate: composeDecorators(decorateEmpty, decorateDate),
    path: 'Fechas.FechaEstimadaFirma',
  },
  'FechaFinal': {
    decorate: composeDecorators(decorateEmpty, decorateDate),
    path: 'Fechas.FechaFinal',
  },
  'FechaInicio': {
    decorate: composeDecorators(decorateEmpty, decorateDate),
    path: 'Fechas.FechaInicio',
  },
  'FechaPublicacion': {
    decorate: composeDecorators(decorateEmpty, decorateDate),
    path: 'Fechas.FechaPublicacion',
  },
  'FechaPubRespuestas': {
    decorate: composeDecorators(decorateEmpty, decorateDate),
    path: 'Fechas.FechaPubRespuestas',
  },
  'FechaVisitaTerreno': {
    decorate: composeDecorators(decorateEmpty, decorateDate),
    path: 'Fechas.FechaVisitaTerreno',
  },
  'FonoResponsableContrato': {
    decorate: composeDecorators(decorateEmpty, decoratePhoneNumber),
    path: 'FonoResponsableContrato',
  },
  'FuenteFinanciamiento': {
    decorate: composeDecorators(decorateEmpty),
    path: 'FuenteFinanciamiento',
  },
  'Modalidad': {
    decorate: composeDecorators(decorateEmpty, decorateDictionary(Modalities)),
    path: 'Modalidad',
  },
  'MontoEstimado': {
    decorate: composeDecorators(decorateEmpty),
    path: 'MontoEstimado',
  },
  'Nombre': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Nombre',
  },
  'NombreOrganismoComprador': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Comprador.NombreOrganismo',
  },
  'NombreResponsableContrato': {
    decorate: composeDecorators(decorateEmpty),
    path: 'NombreResponsableContrato',
  },
  'NombreResponsablePago': {
    decorate: composeDecorators(decorateEmpty),
    path: 'NombreResponsablePago',
  },
  'NombreUsuarioComprador': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Comprador.NombreUsuario',
  },
  'NumeroAdjudicacion': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Adjudicacion.Numero',
  },
  'NumeroOferentesAdjudicacion': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Adjudicacion.NumeroOferentes',
  },
  'Obras': {
    decorate: composeDecorators(decorateEmpty, decorateBoolean),
    path: 'Obras',
  },
  'PeriodoTiempoRenovacion': {
    decorate: composeDecorators(decorateEmpty),
    path: 'PeriodoTiempoRenovacion',
  },
  'ProhibicionContratacion': {
    decorate: composeDecorators(decorateEmpty),
    path: 'ProhibicionContratacion',
  },
  'RegionUnidadComprador': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Comprador.RegionUnidad',
  },
  'RutUnidadComprador': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Comprador.RutUnidad',
  },
  'SubContratacion': {
    decorate: composeDecorators(decorateEmpty, decorateBoolean),
    path: 'SubContratacion',
  },
  'TiempoDuracionContrato': {
    decorate: composeDecorators(decorateEmpty),
    path: 'TiempoDuracionContrato',
  },
  'Tipo': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Tipo',
  },
  'TipoAdjudicacion': {
    decorate: composeDecorators(decorateEmpty, decorateDictionary(Awards)),
    path: 'Adjudicacion.Tipo',
  },
  // 'TipoPago': {}, // undocumented
  'TomaRazon': {
    decorate: composeDecorators(decorateEmpty),
    path: 'TomaRazon',
  },
  // 'UnidadTiempoContratoLicitacion': {}, // undocumented
  'UnidadTiempoDuracionContrato': {
    decorate: composeDecorators(decorateEmpty, decorateDictionary(TimeUnits)),
    path: 'UnidadTiempoDuracionContrato',
  },
  'UrlActaAdjudicacion': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Adjudicacion.UrlActa',
  },
  // 'ValorTiempoRenovacion': {}, // undocumented
  // 'VisibilidadMonto': {}, // undocumented
};
