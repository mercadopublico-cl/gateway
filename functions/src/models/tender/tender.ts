import { getNestedProperty } from '../../modules/utils';
import { ITenderFields, ITenderFieldsDecorators, TenderFieldsDecorators } from './tender.schemas';

type TTenderDecorate = () => ITenderFields | null;

/**
 * The Tender's model.
 */
class Tender {
  public constructor(rawResponse: any) {
    const [rawTender] = rawResponse.Listado || [];

    this.rawTender = rawTender;
  }

  private decorators: ITenderFieldsDecorators = TenderFieldsDecorators;

  private rawTender: any;

  decorate: TTenderDecorate = () => {
    if (!this.rawTender) {
      return null;
    }

    return Object.keys(this.decorators).reduce<ITenderFields>((tender, key) => ({
      ...tender,
      [key]: this.decorators[key].decorate(getNestedProperty(this.rawTender, this.decorators[key].path), 'sin_dato'),
    }), {} as ITenderFields);
  };
}

export default Tender;
