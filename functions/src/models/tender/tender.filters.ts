/**
 * interface ITenderFilters {
 *    --Todas las regiones: "-1"
 *    --Región de Arica y Parinacota: "15"
 *    --Región de Tarapacá: "1"
 *    --Región de Antofagasta: "2"
 *    --Región de Atacama: "3"
 *    --Región de Coquimbo: "4"
 *    --Región de Valparaíso: "5"
 *    --Región Metropolitana de Santiago: "13"
 *    --Región del Libertador General Bernardo O´Higgins: "6"
 *    --Región del Maule: "7"
 *    --Región del Ñuble: "16"
 *    --Región del Biobío: "8"
 *    --Región de la Araucanía: "9"
 *    --Región de Los Ríos: "14"
 *    --Región de los Lagos: "10"
 *    --Región Aysén del General Carlos Ibáñez del Campo: "11"
 *    --Región de Magallanes y de la Antártica: "12"
 *    codigoRegion: string;
 *    compradores: string[];
 *    --Publicado: "1"
 *    --No Publicado: "0"
 *    esPublicoMontoEstimado: string[] | null; // null: all, []: empty 
 *    fechaFin: string | null;
 *    fechaInicio: string | null;
 *    --Sin garantías: "0"
 *    --Fiel cumplimiento del contrato: "1"
 *    --Seriedad de las ofertas: "2"
 *    --Otras garantías: "3"
 *    garantias: string[] | null; // null: all, []: empty
 *    --Todos los estados: "-1"
 *    --Publicada: "5"
 *    --Cerrada: "6"
 *    --Desierta: "7"
 *    --Adjudicada: "8"
 *    --Revocada: "18"
 *    --Suspendida: "19"
 *    idEstado: string;
 *    --Más relevantes primero: "1"
 *    --Próximas a cerrarse: "2"
 *    --Últimas publicadas: "3"
 *    --Mayor monto primero: "7"
 *    --Menor monto primero: "8"
 *    idOrden: string;
 *    --Este mes: "1"
 *    --Próximo mes: "2"
 *    --3 meses o más: "3"
 *    idTipoFecha: string[];
 *    --Todos los tipos: "-1"
 *    --(L1) Licitación Pública Menor a 100 UTM: "1"
 *    --(LE) Licitación Pública Entre 100 y 1000 UTM: "2"
 *    --(LP) Licitación Pública igual o superior a 1.000 UTM e inferior a 2.000 UTM: "3"
 *    --(LQ) Licitación Pública igual o superior a 2.000 UTM e inferior a 5.000 UTM: "24"
 *    --(LR) Licitación Pública igual o superior a 5.000 UTM: "25"
 *    --(LS) Licitación Pública Servicios personales especializados: "23"
 *    --(O1) Licitación Pública de Obras: "30"
 *    --(E2) Licitación Privada Inferior a 100 UTM: "13"
 *    --(CO) Licitación Privada igual o superior a 100 UTM e inferior a 1000 UTM: "9"
 *    --(B2) Licitación Privada igual o superior a 1000 UTM e inferior a 2000 UTM: "10"
 *    --(H2) Licitación Privada igual o superior a 2000 UTM e inferior a 5000 UTM: "26"
 *    --(I2) Licitación Privada Mayor a 5000 UTM: "27"
 *    --(O2) Licitación Privada de Obras: "29"
 *    idTipoLicitacion: string;
 *    montoEstimadoTipo: number[]; // [0]
 *    pagina: number; // 0 and 1 are the same 
 *    proveedores: string[];
 *    registrosPorPagina: string // "10", deprecated?
 *    rubros: string[];
 *    textoBusqueda: string; // "cons"
 * }
 *
 * Example:
 *  --codigoRegion: '-1',
 *  --compradores: [],
 *  --esPublicoMontoEstimado: null,
 *  --fechaFin: null,
 *  --fechaInicio: null,
 *  --garantias: null,
 *  --idEstado: '5',
 *  --idOrden: '1',
 *  --idTipoFecha: [],
 *  --idTipoLicitacion: "-1",
 *  --montoEstimadoTipo: [0],
 *  --pagina: 0,
 *  --proveedores: [],
 *  --registrosPorPagina: 17,
 *  --rubros: [],
 *  --textoBusqueda: 'cons',
 */

export const TenderAmount: Record<string, string> = {
  'no': '0',
  'si': '1',
}

export const TenderSort: Record<string, string> = {
  '1': '1',
  '2': '2',
  '3': '7',
  '4': '8',
  '5': '3',
};

export enum TenderStatus {
  'all' = '-1',
  'adjudicada' = '8',
  'cerrada' = '6',
  'desierta' = '7',
  'publicada' = '5',
  'revocada' = '18',
  'suspendida' = '19',
};

export enum TenderType {
  'all' = '-1',
  'L1' = '1',
  'LE' = '2',
  'LP' = '3',
  'LQ' = '24',
  'LR' = '25',
  'LS' = '23',
  'O1' = '30',
  'E2' = '13',
  'CO' = '9',
  'B2' = '10',
  'H2' = '26',
  'I2' = '27',
  'O2' = '29',
};

export interface ITenderBody {
  codigoRegion: '-1' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | '10' | '11' | '12' | '13' | '14' | '15' | '16';
  compradores: string[];
  esPublicoMontoEstimado: string | null;
  fechaFin: string | null;
  fechaInicio: string | null;
  garantias: null;
  idEstado: '-1' | '5' | '6' | '7' | '8' | '18' | '19';
  idOrden: string;
  idTipoFecha: string[];
  idTipoLicitacion: '-1' | '1' | '2' | '3' | '24' | '25' | '23' | '30' | '13' | '9' | '10' | '26' | '27' | '29';
  montoEstimadoTipo: [0];
  pagina: number;
  proveedores: string[];
  registrosPorPagina: number;
  rubros: string[];
  textoBusqueda: string;
}

export interface ITenderFilters {
  estado?: TenderStatus;
  fecha?: string;
  fechaFin?: string;
  fechaInicio?: string;
  montoPublicado?: 'no' | 'si';
  order?: string;
  region?: ITenderBody['codigoRegion'];
  rubros?: string;
  texto?: string;
  tipo?: TenderType;
}
