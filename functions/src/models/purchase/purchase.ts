import { getNestedProperty } from '../../modules/utils';
import { IPurchaseFields, IPurchaseFieldsDecorators, PurchaseFieldsDecorators } from './purchase.schemas';

type TPurchaseDecorate = () => IPurchaseFields | null;

/**
 * The Purchase's model.
 */
class Purchase {
  public constructor(rawResponse: any) {
    const [rawPurchase] = rawResponse.Listado || [];

    this.rawPurchase = rawPurchase;
  }

  private decorators: IPurchaseFieldsDecorators = PurchaseFieldsDecorators;

  rawPurchase: any;

  decorate: TPurchaseDecorate = () => {
    if (!this.rawPurchase) {
      return null;
    }

    return Object.keys(this.decorators).reduce<IPurchaseFields>((purchase, key) => ({
      ...purchase,
      [key]: this.decorators[key].decorate(getNestedProperty(this.rawPurchase, this.decorators[key].path), 'sin_dato'),
    }), {} as IPurchaseFields);
  };
}

export default Purchase;
