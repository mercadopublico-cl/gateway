import { composeDecorators } from '../../modules/utils';
import {
  decorateBoolean,
  decorateDate,
  decorateDictionary,
  decorateEmpty,
  decorateList,
  decoratePhoneNumber,
} from '../decorators';

const Dispatches = {
  '7': 'Despachar a Dirección de envío',
  '9': 'Despachar según programa adjuntado',
  '12':	'Otra Forma de Despacho',
  '14':	'Retiramos de su bodega',
  '20':	'Despacho por courier o encomienda aérea',
  '21':	'Despacho por courier o encomienda terrestre',
  '22':	'A convenir',
};

const Payments = {
  '1': '15 días contra la recepción de la factura',
  '2': '30 días contra la recepción de la factura',
  '39':	'Otra forma de pago',
  '46':	'50 días contra la recepción de la factura',
  '47':	'60 días contra la recepción de la factura',
};

export interface IPurchaseItemFields {
  'Cantidad': number;
  'Categoria': string;
  'EspecificacionComprador': string;
  'EspecificacionProveedor': string;
  'Moneda': string;
  'PrecioNeto': number;
  'Producto': string;
  'Total': number;
  'TotalCargos': number;
  'TotalDescuentos': number;
  'TotalImpuestos': number;
}

export interface IPurchaseFields {
  'ActividadComprador': string;
  'ActividadProveedor': string;
  'CantidadEvaluacion': number;
  'Cargos': number;
  'CargoContactoComprador': string;
  'CargoContactoProveedor': string;
  'Codigo': string;
  'CodigoLicitacion': string;
  'ComunaProveedor': string;
  'ComunaUnidadComprador': string;
  'Descripcion': string;
  'Descuentos': number;
  'DireccionProveedor': string;
  'DireccionUnidadComprador': string;
  'Estado': string;
  'EstadoProveedor': string;
  'FechaAceptacion': string;
  'FechaCancelacion': string;
  'FechaCreacion': string;
  'FechaEnvio': string;
  'FechaUltimaModificacion': string;
  'FonoContactoComprador': string;
  'FonoContactoProveedor': string;
  'FormaPago': string;
  'Impuestos': number;
  'Items': IPurchaseItemFields[];
  'MailContactoComprador': string;
  'MailContactoProveedor': string;
  'Nombre': string;
  'NombreContactoComprador': string;
  'NombreContactoProveedor': string;
  'NombreOrganismoComprador': string;
  'NombreProveedor': string;
  'NombreSucursalProveedor': string;
  'NombreUnidadComprador': string;
  'Pais': string;
  'PaisComprador': string;
  'PaisProveedor': string;
  'PorcentajeIva': number;
  'PromedioCalificacion': number;
  'RegionProveedor': string;
  'RegionUnidadComprador': string;
  'RutSucursalProveedor': string;
  'RutUnidadComprador': string;
  'TieneItems': string;
  'TipoDespacho': string;
  'Total': number;
  'TotalNeto': number;
}

export interface IPurchaseFieldsDecorators {
  [key: string]: {
    decorate: any;
    path: string;
  };
}

export const PurchaseItemFieldsDecorators: IPurchaseFieldsDecorators = {
  'Cantidad': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Cantidad',
  },
  'Categoria': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Categoria',
  },
  'EspecificacionComprador': {
    decorate: composeDecorators(decorateEmpty),
    path: 'EspecificacionComprador',
  },
  'EspecificacionProveedor': {
    decorate: composeDecorators(decorateEmpty),
    path: 'EspecificacionProveedor',
  },
  'Moneda': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Moneda',
  },
  'PrecioNeto': {
    decorate: composeDecorators(decorateEmpty),
    path: 'PrecioNeto',
  },
  'Producto': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Producto',
  },
  'Total': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Total',
  },
  'TotalCargos': {
    decorate: composeDecorators(decorateEmpty),
    path: 'TotalCargos',
  },
  'TotalDescuentos': {
    decorate: composeDecorators(decorateEmpty),
    path: 'TotalDescuentos',
  },
  'TotalImpuestos': {
    decorate: composeDecorators(decorateEmpty),
    path: 'TotalImpuestos',
  },
};

export const PurchaseFieldsDecorators: IPurchaseFieldsDecorators = {
  'ActividadComprador': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Comprador.Actividad',
  },
  'ActividadProveedor': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Proveedor.Actividad',
  },
  'CantidadEvaluacion': {
    decorate: composeDecorators(decorateEmpty),
    path: 'CantidadEvaluacion',
  },
  'Cargos': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Cargos',
  },
  'CargoContactoComprador': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Comprador.CargoContacto',
  },
  'CargoContactoProveedor': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Proveedor.CargoContacto',
  },
  'Codigo': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Codigo',
  },
  'CodigoLicitacion': {
    decorate: composeDecorators(decorateEmpty),
    path: 'CodigoLicitacion',
  },
  'ComunaProveedor': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Proveedor.Comuna',
  },
  'ComunaUnidadComprador': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Comprador.ComunaUnidad',
  },
  'Descripcion': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Descripcion',
  },
  'Descuentos': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Descuentos',
  },
  'DireccionProveedor': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Proveedor.Direccion',
  },
  'DireccionUnidadComprador': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Comprador.DireccionUnidad',
  },
  'Estado': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Estado',
  },
  'EstadoProveedor': {
    decorate: composeDecorators(decorateEmpty),
    path: 'EstadoProveedor',
  },
  'FechaAceptacion': {
    decorate: composeDecorators(decorateEmpty, decorateDate),
    path: 'Fechas.FechaAceptacion',
  },
  'FechaCancelacion': {
    decorate: composeDecorators(decorateEmpty, decorateDate),
    path: 'Fechas.FechaCancelacion',
  },
  'FechaCreacion': {
    decorate: composeDecorators(decorateEmpty, decorateDate),
    path: 'Fechas.FechaCreacion',
  },
  'FechaEnvio': {
    decorate: composeDecorators(decorateEmpty, decorateDate),
    path: 'Fechas.FechaEnvio',
  },
  'FechaUltimaModificacion': {
    decorate: composeDecorators(decorateEmpty, decorateDate),
    path: 'Fechas.FechaUltimaModificacion',
  },
  'FonoContactoComprador': {
    decorate: composeDecorators(decorateEmpty, decoratePhoneNumber),
    path: 'Comprador.FonoContacto',
  },
  'FonoContactoProveedor': {
    decorate: composeDecorators(decorateEmpty, decoratePhoneNumber),
    path: 'Proveedor.FonoContacto',
  },
  'FormaPago': {
    decorate: composeDecorators(decorateEmpty, decorateDictionary(Payments)),
    path: 'FormaPago',
  },
  'Impuestos': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Impuestos',
  },
  'Items': {
    decorate: composeDecorators(decorateEmpty, decorateList(PurchaseItemFieldsDecorators)),
    path: 'Items.Listado',
  },
  'MailContactoComprador': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Comprador.MailContacto',
  },
  'MailContactoProveedor': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Proveedor.MailContacto',
  },
  'Nombre': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Nombre',
  },
  'NombreContactoComprador': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Comprador.NombreContacto',
  },
  'NombreContactoProveedor': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Proveedor.NombreContacto',
  },
  'NombreOrganismoComprador': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Comprador.NombreOrganismo',
  },
  'NombreProveedor': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Proveedor.Nombre',
  },
  'NombreSucursalProveedor': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Proveedor.NombreSucursal',
  },
  'NombreUnidadComprador': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Comprador.NombreUnidad',
  },
  'Pais': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Pais',
  },
  'PaisComprador': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Comprador.Pais',
  },
  'PaisProveedor': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Proveedor.Pais',
  },
  'PorcentajeIva': {
    decorate: composeDecorators(decorateEmpty),
    path: 'PorcentajeIva',
  },
  'PromedioCalificacion': {
    decorate: composeDecorators(decorateEmpty),
    path: 'PromedioCalificacion',
  },
  'RegionProveedor': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Proveedor.Region',
  },
  'RegionUnidadComprador': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Comprador.RegionUnidad',
  },
  'RutSucursalProveedor': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Proveedor.RutSucursal',
  },
  'RutUnidadComprador': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Comprador.RutUnidad',
  },
  'TieneItems': {
    decorate: composeDecorators(decorateEmpty, decorateBoolean),
    path: 'TieneItems',
  },
  'TipoDespacho': {
    decorate: composeDecorators(decorateEmpty, decorateDictionary(Dispatches)),
    path: 'TipoDespacho',
  },
  'Total': {
    decorate: composeDecorators(decorateEmpty),
    path: 'Total',
  },
  'TotalNeto': {
    decorate: composeDecorators(decorateEmpty),
    path: 'TotalNeto',
  },
};
