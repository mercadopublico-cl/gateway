import format from 'date-fns/format';
import parseISO from 'date-fns/parseISO';
import { getNestedProperty } from '../modules/utils';

type TDecorateBoolean<R = string | number> = (rawBoolean: R) => string; 

/**
 * To decorate boolean values.
 * 
 * @typedef {TDecorateBoolean}
 */
export const decorateBoolean: TDecorateBoolean = rawBoolean => rawBoolean === '0' || rawBoolean === 0 ? 'No' : 'Sí';

type TDecorateDate<D = string> = (rawDate: D) => string; 

/**
 * To decorate date values.
 * 
 * @typedef {TDecorateDate}
 */
export const decorateDate: TDecorateDate = rawDate => format(parseISO(rawDate), 'dd-MM-yyyy HH:mm:ss');

type TDecorateDictionary<D = string | number> = (rawDatum: D) => string;
type TDecorateFactoryDictionary<D = Record<string, string>> = (dictionary: D) => TDecorateDictionary; 

/**
 * To decorate a value from a dictionary.
 * 
 * @typedef {TDecorateFactoryDictionary}
 */
export const decorateDictionary: TDecorateFactoryDictionary = dictionary => rawDatum => {
  const result = dictionary[rawDatum];

  return result || 'sin_documentacion';
};

type TDecorateEmpty<D = any> = (rawDatum: D) => D | string; 

/**
 * To decorate empty' values.
 * 
 * @typedef {TDecorateEmpty} 
 */
export const decorateEmpty: TDecorateEmpty = rawDatum => {
  const sanitizedDatum = typeof rawDatum === 'string' ? rawDatum.trim() : rawDatum;

  if (!sanitizedDatum && sanitizedDatum !== 0) {
    return null;
  }

  return rawDatum;
};

type TDecorateList<LR = any[], LD = any[]> = (rawList: LR) => LD;
type TDecorateFactoryList<DR = any> = (decorators: DR) => TDecorateList;

/**
 * To decorate values from a list.
 * 
 * @typedef {TDecorateFactoryList}
 */
export const decorateList: TDecorateFactoryList = decorators => rawList => {
  return rawList.map(item => {
    return Object.keys(decorators).reduce<any>((tender, key) => ({
      ...tender,
      [key]: decorators[key].decorate(getNestedProperty(item, decorators[key].path), 'sin_dato'),
    }), {});
  });
};

type TDecoratePhoneNumber = (rawPhoneNumber: string) => string;

/**
 * To decorate phone numbers values.
 * 
 * @typedef {TDecoratePhoneNumber}
 */
export const decoratePhoneNumber: TDecoratePhoneNumber = rawPhoneNumber => rawPhoneNumber.replace(/[\-\(\) ]/g, '');
