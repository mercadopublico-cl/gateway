import http from 'http';
import { URL } from 'url';
import { REQUEST_DEBOUNCE_TIME } from '../config/settings';

type TDebounceRequestCallback<R = any> = () => Promise<R>;
type TDebounceRequest<R = any> = (callback: TDebounceRequestCallback, debounceTime?: number) => Promise<R>;

/**
 * To do debounce between requests.
 * 
 * @typedef {TDebounceRequest}
 */
export const debounceRequest: TDebounceRequest = (callback, debounceTime = REQUEST_DEBOUNCE_TIME) =>
  new Promise(resolve => setTimeout(() => resolve(callback()), debounceTime));

type TErrorRequestCallback<R = any> = () => R;
type TErrorRequest<R = any> = (callback: TErrorRequestCallback<R>, errorRule: boolean, errorMessage?: string) => R;

/**
 * To handle requests' errors.
 * 
 * @typedef {TErrorRequest}
 */
export const errorRequest: TErrorRequest = (callback, errorRule, errorMessage = '') => {
  if (errorRule) {
    throw new Error(errorMessage);
  }

  return callback();
};

type TGetRequest<R = any> = (url: string) => Promise<R>;

/**
 * To do HTTP GET requests.
 * 
 * @typedef {TGetRequest}
 */
export const getRequest: TGetRequest = url => {
  const urlInstance = new URL(url);

  return new Promise((resolve, reject) => {
    const request = http.request({
      headers: { 'Content-Type': 'application/json' },
      hostname: urlInstance.hostname,
      method: 'GET',
      path: `${urlInstance.pathname}${urlInstance.search}`,
    }, response => {
      const payload: Buffer[] = [];

      response
        .on('data', chunk => payload.push(chunk as Buffer))
        .on('end', () => resolve(Buffer.concat(payload).toString('utf8')));
    });

    request.on('error', error => reject(error));
    request.end();
  });
};

type TPostRequest<R = any, D = any> = (url: string, data: D) => Promise<R>;

/**
 * To do HTTP POST requests.
 * 
 * @typedef {TPostRequest}
 */
export const postRequest: TPostRequest = (url, data) => {
  const urlInstance = new URL(url);
  const body = JSON.stringify(data);

  return new Promise((resolve, reject) => {
    const request = http.request({
      headers: {
        'Content-Type': 'application/json',
        'Content-Length': Buffer.byteLength(body),
      },
      hostname: urlInstance.hostname,
      method: 'POST',
      path: `${urlInstance.pathname}${urlInstance.search}`,
    }, response => {
      const payload: Buffer[] = [];
  
      response
        .on('data', chunk => payload.push(chunk as Buffer))
        .on('end', () => resolve(Buffer.concat(payload).toString('utf8')));
    });
    
    request.on('error', error => reject(error));
    request.write(body);
    request.end();
  });
};
