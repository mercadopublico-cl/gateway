import { SLICE_LIST_END_INDEX, SLICE_LIST_START_INDEX } from "../config/settings";

type TDecorator<V = any, R = any> = (value: V) => R;
type TComposeDecorators<I = any, R = any, D = string> = (initialInput: I, defaultResult?: D) => R; 
type TComposeFactoryDecorators = (...decorators: TDecorator[]) => TComposeDecorators;

/**
 * To compose decorators and cut the flow if any decoration returns a falsy value (except 0).
 * 
 * @typedef {TComposeFactoryDecorators}  
 */
export const composeDecorators: TComposeFactoryDecorators = (...decorators) => (initialInput, defaultResult = '') => {
  const result = decorators.reduce((value, decorator) => {
    if (!value && value !== 0) {
      return value;
    }

    return decorator(value);
  }, initialInput);

  return result || result === 0 ? result : defaultResult;
};

type TConvertStringToNumber = (input: string, defaultOutput: number) => number;

/**
 * To convert the given string to number.
 * 
 * @typedef {TConvertStringToNumber}
 */
export const convertStringToNumber: TConvertStringToNumber = (input, defaultOutput) => {
  const parsedInput = parseInt(input, 10);

  if (Number.isNaN(parsedInput)) {
    return defaultOutput;
  }

  return parsedInput;
};

type TGetNestedProperty<S = any, R = undefined> = (source: S, path: string) => R;

/**
 * To get a nested property from a literal object source.
 * 
 * @typedef {TGetNestedProperty}
 */
export const getNestedProperty: TGetNestedProperty = (source, path) => {
  const keys = path.split('.').filter(key => key !== '');
  const result = keys.reduce((property, key) => property && property[key] !== 'undefined' ? property[key] : undefined, source);

  return result;
};

type TMakeLog = (key: string, data: Record<string, any>) => void;

/**
 * To make a log.
 *
 * @typedef {TMakeLog}
 */
export const makeLog: TMakeLog = (key, data) => {
  console.log(key, JSON.stringify(data));
};

type TParseDataFromText = (source: string, pattern: RegExp) => string[];

/**
 * To parse data from text using a given pattern.
 * 
 * @typedef {TParseDataFromText}  
 */
export const parseDataFromText: TParseDataFromText = (source, pattern) => {
  const regexInstance = new RegExp(pattern);

  return source.match(regexInstance) || [];
};

type TRecursiveChainFromListCallback<R = any> = (key: string) => Promise<R>;
type TRecursiveChainFromList<R = any> = (
  callback: TRecursiveChainFromListCallback<R>,
  keys: string[],
  records?: R[],
) => Promise<R[]>;

/**
 * To do dynamics promises chains from a list.
 * 
 * @typedef {TRecursiveChainFromList}
 */
export const recursiveChainFromList: TRecursiveChainFromList = (callback, keys, records = []) => {
  const key = keys && keys.shift();
  
  if (!key) {
    return Promise.resolve(records);    
  }

  return callback(key)
    .then(record => new Promise(resolve =>
      resolve(recursiveChainFromList(callback, keys, record ? records.concat(record) : records)),
    ));
};

type TSliceList<A = any[]> = (input: A, startIndex?: string | number, endIndex?: string | number) => A; 

/**
 * To get a portion of the given list.
 * 
 * @typedef {TSliceList}
 */
export const sliceList: TSliceList = (input, startIndex = SLICE_LIST_START_INDEX, endIndex = SLICE_LIST_END_INDEX) => {
  const endLimit = typeof endIndex === 'string' ? convertStringToNumber(endIndex, SLICE_LIST_END_INDEX) : endIndex;
  const startLimit = typeof startIndex === 'string' ? convertStringToNumber(startIndex, SLICE_LIST_START_INDEX) : startIndex;

  if (endLimit < startLimit || 0 > endLimit || 0 > startLimit) {
    return input.slice(SLICE_LIST_START_INDEX, SLICE_LIST_END_INDEX);
  }

  return input.slice(startLimit, endLimit);
};
