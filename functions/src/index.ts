import * as functions from 'firebase-functions';
import express from 'express';
import cors from 'cors';
import routes from './routes';

const app = express();

app.use(express.json());
app.use(cors({ origin: true }));
app.use('/v1', routes);

export const api = functions
  .runWith({ timeoutSeconds: 540 })
  .https.onRequest(app);
