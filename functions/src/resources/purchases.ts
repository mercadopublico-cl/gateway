import { Request, Response } from 'express';
import * as purchasesController from '../controllers/purchases';

type TPurchaseGetByIDResource = (request: Request, response: Response) => void; 

/**
 * The resource to handle the getByID contract.
 * 
 * Example: 2097-241-SE14.
 * 
 * @typedef {TPurchaseGetByIDResource}
 */
export const getByID: TPurchaseGetByIDResource = async (request, response) => {
  try {
    const { purchaseID = '' } = request.params || {};
    const collection = await purchasesController.getByID(purchaseID);

    response.json(collection);
  } catch (error) {
    response
      .status(500)
      .json({ message: error.message });
  }
};
