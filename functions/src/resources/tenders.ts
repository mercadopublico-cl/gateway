import { Request, Response } from 'express';
import * as tendersController from '../controllers/tenders';

type TTenderGetFromFeedResource = (request: Request, response: Response) => void;

/**
 * The resource to handle the getFromFeed contract.
 * 
 * Example: 1035384.
 * 
 * @typedef {TTenderGetFromFeedResource}
 */
export const getFromFeed: TTenderGetFromFeedResource = async (request, response) => {
  try {
    const filters = request.query || {};
    const { orgID = '' } = request.params || {};
    const collection = await tendersController.getFromFeed(orgID, filters);
    
    response.json(collection);
  } catch (error) {
    response
      .status(500)
      .json({ message: error.message });
  }
};

type TTenderGetFromPortalResource = (request: Request, response: Response) => void;

/**
 * The resource to handle the getFromPortal contract.
 * 
 * @typedef {TTenderGetFromPortalResource}
 */
export const getFromPortal: TTenderGetFromPortalResource = async (request, response) => {
  try {
    const filters = request.query || {};
    const collection = await tendersController.getFromPortal(filters);

    response.json(collection);
  } catch (error) {
    response
      .status(500)
      .json({ message: error.message });
  }
};
