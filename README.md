# MERCADO PÚBLICO GATEWAY

_URL base_: https://us-central1-asesoria-mp.cloudfunctions.net/

## LICITACIONES

### `api/v1/tenders/portal`

Búsqueda de licitaciones mediante el Portal de Mercado Público y obtención de el detalle de cada una mediante la API de Mercado Público. Todos los parámetros son opcionales y son especificados como `query` en la URL.

#### Parámetros

- `estado`

  Se debe especificar solo un valor a la vez. Si no se específica, la búsqueda es por todos los valores.

  **Valores**: `adjudicada`, `cerrada`, `desierta`, `publicada`, `revocada` o `suspendida`.

  _Ejemplo_:

  ```bash
  # Buscar licitaciones publicadas.
  https://us-central1-asesoria-mp.cloudfunctions.net/api/v1/tenders/portal?estado=publicada
  ```

- `fechaFin`

  Se debe especificar solo un valor a la vez en formato `Año-Mes-Día` (`YYYY-MM-DD`). Si se específica este valor también debe ser especificado el parámetro `fechaInicio`.

  _Ejemplo_:

  ```bash
  # Buscar licitaciones entre el primero de Marzo del 2020 y el 15 de Abril del 2020.
  https://us-central1-asesoria-mp.cloudfunctions.net/api/v1/tenders/portal?fechaFin=2020-04-15&fechaInicio=2020-03-01
  ```

- `fechaInicio`

  Se debe especificar solo un valor a la vez en formato `Año-Mes-Día` (`YYYY-MM-DD`). Si se específica este valor también debe ser especificado el parámetro `fechaFin`.

  _Ejemplo_:

  ```bash
  # Buscar licitaciones entre el primero de Marzo del 2020 y el 15 de Abril del 2020.
  https://us-central1-asesoria-mp.cloudfunctions.net/api/v1/tenders/portal?fechaFin=2020-04-15&fechaInicio=2020-03-01
  ```

- `indiceFin`

  Se debe especificar un número entero. Si no se específica, la búsqueda trae los resultados hasta el indice 10. Si se específica este valor también debe ser especificado el parámetro `indiceInicio`.

  **Valores**: número entero mayor a `indiceInicio`.

  _Ejemplo_:

  ```bash
  # Buscar licitaciones y traer los resultados entre los indices 15 y 30.
  https://us-central1-asesoria-mp.cloudfunctions.net/api/v1/tenders/portal?indiceInicio=15&indiceFin=30
  ```

- `indiceInicio`

  Se debe especificar un número entero. Si no se específica, la búsqueda trae los resultados desde el indice 0. Si se específica este valor también debe ser especificado el parámetro `indiceFin`.

  **Valores**: número entero mayor a cero y menor que `indiceFin`.

  _Ejemplo_:

  ```bash
  # Buscar licitaciones y traer los resultados entre los indices 15 y 30.
  https://us-central1-asesoria-mp.cloudfunctions.net/api/v1/tenders/portal?indiceInicio=15&indiceFin=30
  ```

- `montoPublicado`

  Se debe especificar solo un valor a la vez. Si no se específica, la búsqueda es por todos los valores.

  **Valores**: `no` o `si`.

  _Ejemplo_:

  ```bash
  # Buscar licitaciones cuyo monto esta publicado.
  https://us-central1-asesoria-mp.cloudfunctions.net/api/v1/tenders/portal?montoPublicado=si
  ```

- `orden`

  Se debe especificar un solo valor a la vez. Si no se específica, la búsqueda es por el valor `5`.

  **Valores**:

  | Valor | Significado |
  |-|-|
  | 1 | Más relevantes primero |
  | 2 | Próximas a cerrarse |
  | 3 | Mayor monto primero |
  | 4 | Menor monto primero |
  | 5 | Ultimas publicadas |

  _Ejemplo_:

  ```bash
  # Buscar licitaciones por orden de ultimas publicadas.
  https://us-central1-asesoria-mp.cloudfunctions.net/api/v1/tenders/portal?orden=5
  ```

- `region`

  Se debe especificar solo un valor a la vez. Si no se específica, la búsqueda es por todos los valores.

  **Valores**: `1`, `2`, `3`, `4`, `5`, `6`, `7`, `8`, `9`, `10`, `11`, `12`, `13`, `14`, `15` o `16`.

  _Ejemplo_:

  ```bash
  # Buscar licitaciones en la Región del Maule.
  https://us-central1-asesoria-mp.cloudfunctions.net/api/v1/tenders/portal?region=7
  ```

- `rubros`

  Se debe especificar uno o varios valores separados por coma.

  **Valores**: códigos de los rubros.

  ```bash
  # Buscar licitaciones del rubro 72131600.
  https://us-central1-asesoria-mp.cloudfunctions.net/api/v1/tenders/portal?rubros=72131600

  # Buscar licitaciones del rubro 72131600, 72131700 y 72131500.
  https://us-central1-asesoria-mp.cloudfunctions.net/api/v1/tenders/portal?rubros=72131600,72131700,72131500
  ```

- `texto`

  Se debe especificar texto sin espacios ni caracteres especiales.

  _Ejemplo_:

  ```bash
  # Buscar licitaciones que contengan la palabra "construcción".
  https://us-central1-asesoria-mp.cloudfunctions.net/api/v1/tenders/portal?texto=construcción
  ```

- `tipo`

  Se debe especificar solo un valor a la vez. Si no se específica, la búsqueda es por todos los valores.

  **Valores**: `L1`, `LE`, `LP`, `LQ`, `LR`, `LS`, `O1`, `E2`, `CO`, `B2`, `H2`, `I2` o `O2`.

  _Ejemplo_:

  ```bash
  # Buscar licitaciones de tipo LE.
  https://us-central1-asesoria-mp.cloudfunctions.net/api/v1/tenders/portal?tipo=LE
  ```

- `totalMuestra`

  Se debe especificar un número entero. Si no se específica, se tomarán solo los primeros 100 resultados del Portal de Mercado Público.

  **Valores**: número entero mayor a cero.

  _Ejemplo_:

  ```bash
  # Buscar dentro de las 50 primeras licitaciones del Portal.
  https://us-central1-asesoria-mp.cloudfunctions.net/api/v1/tenders/feed/1035384?totalMuestra=50

  # Buscar dentro de las 75 primeras licitaciones del Portal y traer los resultados entre los indices 15 y 20.
  https://us-central1-asesoria-mp.cloudfunctions.net/api/v1/tenders/feed/1035384?totalMuestra=75&indiceInicio=15&indiceFin=20
  ```

#### Más Ejemplos

```bash
# Buscar licitaciones publicadas de tipo LE en la Región del Maule.
https://us-central1-asesoria-mp.cloudfunctions.net/api/v1/tenders/portal?estado=publicada&region=7&tipo=LE
```

```bash
# Buscar licitaciones que tengan la palabra "aseo" y sean de tipo L1 en todo Chile.
https://us-central1-asesoria-mp.cloudfunctions.net/api/v1/tenders/portal?texto=aseo&tipo=L1
```

```bash
# Buscar licitaciones publicadas en la Región del Maule cuyos rubros sean 72131600, 72131700 y 72131500.
https://us-central1-asesoria-mp.cloudfunctions.net/api/v1/tenders/portal?estado=publicada&region=7&rubros=72131600,72131700,72131500
```

#### Consideraciones

- Las respuestas pueden tardar ya que la API de Mercado Público no permite solicitudes simultáneas o concurrentes, por lo que la obtención del detalle de cada licitación es un proceso lento.

- La búsqueda puede tardar hasta 9 minutos, luego de ese tiempo el servidor cortará la conexión y no habrá resultados.

- Debido a las limitaciones para obtener datos desde Mercado Público, se recomendienda un uso inteligente de todos los parámetros en especial de `totalMuestra`, `indiceInicio` e `indiceFin`:
  
  - Si el número de resultados a obtener desde el Portal es alto, la consulta tardará más.
  - Si el rango dado entre indices es alto, la consulta tardará más.

- Si se realiza una búsqueda por fechas, los demás parámetros pierden relevancia.

- Los campos de una licitación pueden tener el valor `sin_dato` si es que el campo es vacío, y un valor `sin_documentacion` si es que el campo no esta documentado en la API de Mercado Público.

### `api/v1/tenders/feed/{OrgCode}`

Búsqueda de licitaciones mediante el RSS Feed de Mercado Público y obtención de el detalle de cada una mediante la API de Mercado Público. Los parámetros especificados como `pathname` de la URL son obligatorios. Los parámetros especificados como `query` en la URL son opcionales.

#### Parámetros

- `OrgCode`

  Se debe especificar el código del feed.

  _Ejemplo_:

  ```bash
  # Buscar licitaciones desde el RSS Feed cuyo OrgCode es 1035384.
  https://us-central1-asesoria-mp.cloudfunctions.net/api/v1/tenders/feed/1035384
  ```

- `indiceFin`

  Se debe especificar un número entero. Si no se específica, la búsqueda trae los resultados hasta el indice 10. Si se específica este valor también debe ser especificado el parámetro `indiceInicio`.

  **Valores**: número entero mayor a `indiceInicio`.

  _Ejemplo_:

  ```bash
  # Buscar licitaciones y traer los resultados entre los indices 15 y 30.
  https://us-central1-asesoria-mp.cloudfunctions.net/api/v1/tenders/feed/1035384?indiceInicio=15&indiceFin=30
  ```

- `indiceInicio`

  Se debe especificar un número entero. Si no se específica, la búsqueda trae los resultados desde el indice 0. Si se específica este valor también debe ser especificado el parámetro `indiceFin`.

  **Valores**: número entero mayor a cero y menor que `indiceFin`.

  _Ejemplo_:

  ```bash
  # Buscar licitaciones y traer los resultados entre los indices 15 y 30.
  https://us-central1-asesoria-mp.cloudfunctions.net/api/v1/tenders/feed/1035384?indiceInicio=15&indiceFin=30
  ```

- `totalMuestra`

  Se debe especificar un número entero. Si no se específica, se tomarán solo los primeros 100 resultados del RSS Feed de Mercado Público.

  **Valores**: número entero mayor a cero.

  _Ejemplo_:

  ```bash
  # Buscar dentro de las 50 primeras licitaciones del RSS Feed.
  https://us-central1-asesoria-mp.cloudfunctions.net/api/v1/tenders/feed/1035384?totalMuestra=50

  # Buscar dentro de las 75 primeras licitaciones del RSS Feed y traer los resultados entre los indices 15 y 20.
  https://us-central1-asesoria-mp.cloudfunctions.net/api/v1/tenders/feed/1035384?totalMuestra=75&indiceInicio=15&indiceFin=20
  ```

#### Consideraciones

- Las respuestas pueden tardar ya que la API de Mercado Público no permite solicitudes simultáneas o concurrentes, por lo que la obtención del detalle de cada licitación es un proceso lento.

- La búsqueda puede tardar hasta 9 minutos, luego de ese tiempo el servidor cortará la conexión y no habrá resultados.

- Debido a las limitaciones para obtener datos desde Mercado Público, se recomienda un uso inteligente de los parámetros `indiceInicio` e `indiceFin` de forma inteligente:
  
  - Si el rango dado entre indices es alto, la consulta tardará más.

- Los campos de una licitación pueden tener el valor `sin_dato` si es que el campo es vacío, y un valor `sin_documentacion` si es que el campo no esta documentado en la API de Mercado Público.

## ORDENES DE COMPRA

### `api/v1/purchases/{CodigoOrden}`

Búsqueda de una orden de compra. Los parámetros son obligatorios y son especificados como `pathname` de la URL.

#### Parámetros

- `CodigoOrden`

  Se debe especificar el código de la orden de compra.

  _Ejemplo_:

  ```bash
  # Buscar orden de compra cuyo código es 2097-241-SE14.
  https://us-central1-asesoria-mp.cloudfunctions.net/api/v1/purchases/2097-241-SE14
  ```

---

## RECURSOS

- [API Licitaciones de Mercado Público](http://api.mercadopublico.cl/modules/Licitacion.aspx)
- [API Ordenes de Compra de Mercado Público](http://api.mercadopublico.cl/modules/OrdenCompra.aspx)

---
© 2020
